;;; moformat.el --- Moritz text formatting functions

;; Copyright (C) 2011 Free Software Foundation, Inc.

;; Author: Marcos G Moritz <marcosmoritz@gmail.com>
;; Version: 0.0.1
;; Package-Requires: ((org "9.3.6"))
;; Keywords: format, whitespace
;; URL: https://mo-profile.com/moformat

;;; Commentary:
;; This package groups together formatting functions

;; This is a package with text formatting functions
;; to be used in any major mode

(require 'org)

(defun moformat--remove-duplicate-whitespaces-in-string (string)
  (replace-regexp-in-string "\\s-+" " " string))

(defun moformat-remove-duplicate-whitespaces (beg end)
  "Remove all doubled whitespaces"
  (interactive (list (region-beginning) (region-end)))
  (save-excursion
    (goto-char beg)
    (while (re-search-forward "\\s-+" end t)
      (replace-match " "))
    (delete-trailing-whitespace)))

(defun moformat-delete-duplicate-blank-lines (*begin *end)
  (interactive
   (if (region-active-p)
       (list (region-beginning) (region-end))
     (list (point-min) (point-max))))
  (save-excursion
    (goto-char *begin)
    (while (re-search-forward "\n\n\n+" *end "noerror")
      (replace-match "\n\n"))))

(defun moformat-sql (beg end)
  "Requires sqlformat command. pip3 install sqlparse"
  (interactive "r")
  (shell-command-on-region beg end "sqlformat - --reindent --keywords upper --use_space_around_operators" nil t))

(defun moformat-org-table-to-csv ()
  (interactive)
  (unless (org-at-table-p) (user-error "No table at point"))
  (org-table-align)	       ; Make sure we have everything we need.
  (let ((format "orgtbl-to-csv"))
    (unless format
	    (let* ((deffmt-readable
		           (replace-regexp-in-string
		            "\t" "\\t"
		            (replace-regexp-in-string
		             "\n" "\\n"
		             (or "orgtbl-to-csv"
		                 org-table-export-default-format)
		             t t) t t)))
	      (setq format "orgtbl-to-csv")))
    (if (string-match "\\([^ \t\r\n]+\\)\\( +.*\\)?" format)
	      (let ((transform (intern (match-string 1 format)))
		          (params (and (match-end 2)
			                     (read (concat "(" (match-string 2 format) ")"))))
		          (table (org-table-to-lisp
			                (buffer-substring-no-properties
			                 (org-table-begin) (org-table-end)))))
	        (unless (fboundp transform)
	          (user-error "No such transformation function %s" transform))
          (save-excursion
            (kill-region (org-table-begin) (org-table-end))
            (insert (funcall transform table params) "\n"))
	        (message "Export done."))
	    (user-error "TABLE_EXPORT_FORMAT invalid"))))

(provide 'moformat)
